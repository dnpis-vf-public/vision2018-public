ValueFactory presents
![logo](slide/assets/logo.png)
#### 未来のあたりまえはここから始まる
### 2018/03/19 Mon
東京都新宿区市谷左内町21番地
DNP左内町第4ビル
[dnpis-valuefactory@mail.dnp.co.jp](dnpis-valuefactory@mail.dnp.co.jp)

---

### ValueFactory とは

2017/10 時点

<img src="slide/assets/poster.png" width=35%>

+++

### ValueFactory とは

- 100万 + 20万 × 4チーム |
- 自分たちで決めたアイディア、プロダクト |
- 自分たちで決めたプロセス、テクノロジー |
- これまでの会社に対する危機感/不安/不満 |
- これからの会社に対する期待 |
- 私たちなりの行動 |

+++

### Vision Day 2018 とは

- ValueFactory の集大成 |
- どんなイベントだと期待していますか？ |
- 思ってることのおそらくすべてが正解です |

+++

### Vision Day 2018 とは

- 変革の第一歩、自己組織化のはじまり |
- 成長と学びのシェア |
- "会社とは組織とは仕事とはこうあるべき" |
    - 固定観念の打破 |
    - 楽しさドリブン |
    - エンジニア、デベロッパーマインド |

+++

### Vision Day 2018 とは

私たちが個の観点において<br/>
私たちなりの Vision を発信し<br/>
シェアする場です

---

### タイムテーブル

+++

## 未来のあたりまえは
## ここから始まる

+++

<img src="slide/assets/dev/2_Flat_logo_on_transparent_243x69.png" width=300px height=70px>
<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|12:30|イントロダクション|

+++

<img src="slide/assets/dev/2_Flat_logo_on_transparent_243x69.png" width=300px height=70px>

||||
|---|:-:|
|13:00|各チーム/有志展示|

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|13:30|Engineer Mr.Kiyomoto の Deap な講座|

- AI of Diamond の清本さん、小縄さんセッション |
- python の Deap をネタに "遺伝的アルゴリズム" を面白おかしく語ります |

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|14:30|パネルディスカッション<br/>"IT × ハタヘン"<br/>業務小林さん/SS安田さん/ICT三苫さん<br/>ファシリテーター 業務入倉さん|

- 自身や周辺でのイケてるハタヘンを紹介します |
- "私たちが実現したい働き方とは？" についてディスカッションで掘り下げます |
- 果たして見つけ出した課題を IT でハック出来るのか！？ |

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|15:30|<img src="http://www.atmarkit.co.jp/fdotnet/t-interview/unisys_af/unisys03.jpg" width=100px> <br/>日本ユニシス 尾島 良司 氏<br/>深層学習ってこんなにカンタン！<br/>顔認識＆ダメ絶対音感|

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|16:00|<img src="https://event.shoeisha.jp/static/images/speaker/1622/15c1_ichitani_toshihito.png" width=100px> <br/>ギルドワークス 市谷 聡啓 氏<br/>カイゼン・ジャーニー <br/>〜たった一人からはじめて、<br/>「越境」するチームをつくるまで〜|

+++

## ここから各チームの
## 発表セッションです!!

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|16:30|Beacon<br/>アクティブRFIDタグ<br/>- Beaconを用いたソリューション –<br/>|

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|16:50|FirstPenguin<br/>問合せ対応チャットボット基盤|

+++

# Special Guest!!

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|17:10|キリンビジネスシステム<br/>バリューラボ<br/>チャットボットチーム|

+++

# HeadLiner!!

+++

<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|17:30|AI of Diamond<br/>スポ少 車出し と AI|

+++

<img src="slide/assets/dev/2_Flat_logo_on_transparent_243x69.png" width=300px height=70px>
<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|17:50|キリンビールで乾杯！|

+++

<img src="slide/assets/dev/2_Flat_logo_on_transparent_243x69.png" width=300px height=70px>
<img src="slide/assets/learn/2_Flat_logo_on_transparent_207x71.png" width=300px height=70px>

||||
|---|:-:|
|18:00|会場懇親会|

- 運営と有志で簡単な撤収も並行で |
- 104AB は明日 10:00 までは押えてます |

---

<a href="https://gitlab.com/dnpis-vf-public/vision2018-public/raw/master/slide/timetable/slide2.PNG"><img src="https://gitlab.com/dnpis-vf-public/vision2018-public/raw/master/slide/timetable/slide2.PNG" width=40%></a>

---

### ちょっと疲れたな
### ちょっと閃いたので
### コード書きたいな
#### と思ったら...

+++

<iframe src="https://www.google.com/maps/embed?pb=!4v1519895104191!6m8!1m7!1spzlOIvG_BClsaARoQ9XMeg!2m2!1d35.69318725584248!2d139.7347562247843!3f180.64000548643313!4f-4.019193586034518!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

+++

<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-9/15726889_1200722190024668_3877461932290674819_n.jpg?oh=ace50f75ba34da430281e2e3673f7e85&oe=5B0C2D30" width=200px>

http://kitchen.lowp.jp/

##### 休憩、ワークにどうぞ

- 13:00～16:30 B1F貸切 |
- Free Drink (Coffee) |
- Free Wifi |
- 電源あり |


---

### 運営スタッフ

困ったらこの人たちに聞いてください

+++

### 運営スタッフ

<img src="slide/assets/takarada.png" width=200px>

||
|---|:-:|
| 寳田 雅文 | masafumi takarada <br/> [宝田雅文@Facebook](https://www.facebook.com/100002127214744) <br/> [ma1979@GitHub](https://github.com/ma1979) <br/> [ma1979@GitLab](https://gitlab.com/ma1979)|

+++

### 運営スタッフ

<img src="slide/assets/kobayashi.png" width=200px>

||
|---|:-:|
| 小林 竜哉 | tatsuya kobayashi <br/> [tkey-dll@GitLab](https://gitlab.com/tkey-dll)|

+++

### 運営スタッフ

<img src="slide/assets/kawano.png" width=200px>

||
|---|:-:|
| 河野 智晃 | tomoaki kawano <br/> [2017TK@GitLab](https://gitlab.com/2017TK)|

+++

### 運営スタッフ

<img src="slide/assets/fuji.png" width=200px>

||
|---|:-:|
| 冨士原 章 | akira fujihara <br/> [afujihara@GitLab](https://gitlab.com/afujihara)|

---

### 見どころ

- 各チームより
    - FirstPenguin
        - AI
        - Knowledge
    - AI of Diamond
    - Beacon

+++

### 見どころ

- 運営より
- 有志より

---

## One More Thing...

---

<img src="slide/assets/otona/2_Flat_logo_on_transparent_115x73.png" width=200px>

|||
|---|:-:|
|19:00|パーティースペース パティア市ヶ谷|

- 全貸切 |

<img src="https://uds.gnst.jp/rest/img/bt2s8dhs0000/s_0n8m.jpg?t=1495387255" width=200px>

---

# 最後に

---

# Enjoy
# Yourself!!
